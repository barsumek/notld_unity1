﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public int numberOfPresets = 2;
    public Text concentration_text;
    //Duration of a shake
    public float duration = 0.2f;
    //Shake magnitude
    public float magnitude = 0.5f;
    public float sadTime = 7f;
    //Piggy Animator
    public Animator anim;
    public SpriteRenderer dialog;
    //List of dialogs
    public Sprite[] dialogs;

    //Index of a preset
    private int preset;
    private List<GameObject> papers;
    //Number of papers on the table
    private int level = 3;
    //Sequence entered by player
    private List<int> playerSeq = new List<int>();
    //Winning sequence
    private List<int> winSeq = new List<int>();
    private int clickCounter = 0;
    private int prevRand = -1;
    private GameObject sequenceLocks;
    private int goodClicks;
    private float startPositionX;
    private Dictionary<int, float> startPositionXDict = new Dictionary<int, float>()
    {
        { 3, -1.7f},
        { 4,-2.5f },
        { 5,-3.3f}
    };
    private int state = 0;
    private Vector3 originalCamPos;


    public int GoodClicks
    {
        get { return this.goodClicks; }
        set { goodClicks = value; }
    }

    public int Preset
    {
        get { return this.preset; }
    }


	void Awake()
    {
        //Get the camera position for shake method
        originalCamPos = Camera.main.transform.position;
        
        //Set the starting position of the first paper piece (level= 3)
        startPositionX = startPositionXDict[level];

        goodClicks = 0;

        //List of all the papers from the preset
        papers = new List<GameObject>();

        //Pick one of the presets
        preset = Random.Range(1, numberOfPresets);

        //Adding lock prefabs to a list
        for(int i=1;i<=5;i++)
        {
            papers.Add(Resources.Load("Papers/1/"+preset + i + "PaperPiece") as GameObject) ;
        }
        NextLock();
    }

    void Start()
    {
        StartCoroutine(DisplayDialog(1));
    }

    void Update()
    {
        concentration_text.text = "Concentration: " + goodClicks.ToString();
        CheckGameOver();

        if (state != 0 && GetComponent<Timer>().Current_timer < sadTime)
        {
            state = 1;
            anim.SetInteger("State", state);
            StartCoroutine(DisplayDialog(3));
        }
        else
        {
            state = 0;
            anim.SetInteger("State", state);
        }
    }

    public void onClick(int index)
    {
        AudioManager.instance.RandomizeSfx(AudioManager.instance.clips[0]);
        playerSeq.Add(index);
        //Checking if last Player's click is on the winning combination
        if (playerSeq.Last() == winSeq[clickCounter])
        {
            sequenceLocks.transform.GetChild(clickCounter).gameObject.GetComponent<SpriteRenderer>().color = Color.green;
            //If we have entered correctly the last number we win
            if (clickCounter == level-1)
            {
                anim.SetTrigger("Thumbs");
                
                GetComponent<Timer>().Current_timer += 3.0f;
                
                clickCounter = -1;
                goodClicks++;

                if (goodClicks == 4)
                {
                    IncreaseDifficulty();
                }
                else if (goodClicks == 8)
                {
                    IncreaseDifficulty();
                }

                NextLock();
            } 
        }
        else
        {
            anim.SetTrigger("Angry");
            StartCoroutine(DisplayDialog(2));
            StartCoroutine(Shake());
            NextLock();
            clickCounter = -1;
           
        }

        clickCounter += 1;
    }

   

    public void NextLock()
    {
        //Clear the previous sequence
        if (sequenceLocks != null)
        {
            Destroy(sequenceLocks);
        }

        sequenceLocks = new GameObject("Sequence Locks");
        //Index of a first paper
        int paperIndex = Random.Range(0, papers.Count);

        //This section ensures that the sequence will differ from the previous one
        while (paperIndex == prevRand)
        {
            paperIndex = Random.Range(0, papers.Count);
        }
        prevRand = paperIndex;

        //Position of a first paper
        float prevXPos = startPositionX;

        //The space between next papers. It's set to 0 because of a first object
        float deltaXPos = 0;

        //Generate all the corresponding papers pieces
        GameObject paperPiece = papers[paperIndex];

        //Clear the lists for new generated papers
        winSeq.Clear();
        playerSeq.Clear();

        //Instantiate as many papers as the level says
        for (int i = 0; i < level; i++)
        {
            //Add the paper to a winning sequence
            winSeq.Add(papers.IndexOf(paperPiece));

            //Instantiating object at the given position
            var elemLock = (GameObject) Instantiate(paperPiece, new Vector3(prevXPos+deltaXPos, 1),Quaternion.identity);
            
            //Parenting the object in order to destroy the whole sequence
            elemLock.transform.parent = sequenceLocks.transform;
   
            prevXPos += deltaXPos;
            deltaXPos = 1.8f;

            //Picking one end node from all the end nodes of the instantiated object
            Nodes[] end = paperPiece.GetComponent<PickLockBlock>().endingNodes;
            Nodes select_end = end[Random.Range(0, end.Length)];

            //Now we've got to find a corresponding start_node
            //Creating a list for objects which correspond to end node
            List <GameObject> endNodeObjects = new List<GameObject>();

            //Searching gameObjects and adding them to list
            foreach (GameObject elem in papers)
            {
                foreach (Nodes n in elem.GetComponent<PickLockBlock>().startingNodes)
                {
                    if (n.Equals(select_end))
                    {
                        endNodeObjects.Add(elem);
                    }
                }
                
            }
            //Return a random corresponding game object
            paperPiece = endNodeObjects[Random.Range(0, endNodeObjects.Count)];  
        }

       

    }
   
    public void CheckGameOver()
    {
        if (GetComponent<Timer>().Current_timer<=0)
        {
            SceneManager.LoadScene(2);
            PlayerPrefs.SetInt("result", goodClicks);
        }
    }

    void IncreaseDifficulty()
    {
        level++;
        Debug.Log(level.ToString());
        startPositionX = startPositionXDict[level];
    }
    /// <summary>
    /// Shakes the camera
    /// </summary>
    /// <returns></returns>
    IEnumerator Shake()
    {

        float elapsed = 0.0f;

       

        while (elapsed < duration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;

            Camera.main.transform.position = new Vector3(x, y, originalCamPos.z);

            yield return null;
        }

        Camera.main.transform.position = originalCamPos;
    }

    IEnumerator DisplayDialog(int spriteNumber)
    {
       
        dialog.sprite = dialogs[spriteNumber];
        yield return new WaitForSeconds(2f);
        dialog.sprite = null;
        yield return null;
    }

}
