﻿using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour {
    public float timeInterval = 1.0f;
    public float speed = 2.0f;

    public void RunColorChange()
    {
        StartCoroutine(ColorChangeRoutine());
    }

    private IEnumerator ColorChangeRoutine()
    {
        float timer = 0.0f;
        Color32 color = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);

        while (timer < timeInterval)
        {
            timer += Time.deltaTime * speed;
            Camera.main.backgroundColor = Color32.Lerp(Camera.main.backgroundColor, color, timer);

            yield return null;
        }

        // On finish recursively call the same routine
        StartCoroutine(ColorChangeRoutine());
    }
}
