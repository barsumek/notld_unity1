﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour {
    public Text t;

    void Start()
    {
        t.text = "YOUR CONCENTRATION WAS: " + PlayerPrefs.GetInt("result").ToString();
    }

	public void Restart()
    {
        SceneManager.LoadScene(1);
    }

    public void GameOver()
    {
        Application.Quit();
    }
}
