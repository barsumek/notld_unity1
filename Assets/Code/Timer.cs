﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    //inital time
    public float start_time = 20f;
    //Ui element to show our timer
    public Text timerUI;
    public bool startTimer = false;
    //Currrent time
    private float current_timer;
    public float Current_timer
    {
        get { return this.current_timer; }
        set { current_timer = value; }
    }
    private int minutes;
    private int seconds;
    private string time_string;
    

    void Start()
    {
        current_timer = start_time;
    }

    void Update()
    {
        if (startTimer)
        {
            current_timer -= Time.deltaTime;
            if (current_timer <= 0.0f)
            {
                current_timer = 0.0f;
            }
            if (current_timer <= 5.0f)
            {
                timerUI.color = Color.red;
            }
            else
            {
                timerUI.color = Color.black;
            }
            
        }
        minutes = Mathf.FloorToInt(current_timer / 60F);
        seconds = Mathf.FloorToInt(current_timer - minutes * 60);
        time_string = string.Format("{0:00}:{1:00}", minutes, seconds);
        timerUI.text = time_string;
    }
}
